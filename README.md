dd-dashboards-backup
====================

#### description

Makes local git-versioned backups of every dashboard from every accessible orgs using the Datadog API.

Reference : https://docs.datadoghq.com/api/

#### usage

###### using docker-compose

    # docker-compose up

###### using bash

This method relies on the **local copy** of `dd-dashboards-backup.sh`

    # bash dd-dashboards-backup.sh

Note : As shown below, this method prompts the user to ensure the adequate account is being used prior to starting processing.

    # bash dd-dashboards-backup.sh
    "DogFood (xyz)"
    confirm account : press any key to continue or ctrl-c to exit

###### using curl

This method relies on the **remote copy** *(gitlab.com snippet)* of `dd-dashboards-backup.sh`

    # source secrets.ini
    # curl -Lkqs https://gitlab.com/-/snippets/2027000/raw | bash

Note : This would run locally *(non-containerized)* so requires locally available binary dependencies as well as environment variables *(see next section, credentials management)*.

###### credentials management

Example secrets file exists in `secrets/sample`.

Defaults to parsing `secrets/dogfood`.

Selecting the desired *"profile"* is achieved by setting the `ENV` environment variable.

    # ENV=sample docker-compose up

The above would lookup credentials in `secrets/sample`

    # ENV=demo docker-compose up

The above would lookup credentials in `secrets/demo`

    # ENV=dogfood docker-compose up

The above would lookup credentials in `secrets/dogfood`

Note : If `ENV` is **unset**, this is the default.

###### example output

    # docker-compose --log-level ERROR up
    Starting dd-dashboards-backup ... done
    Attaching to dd-dashboards-backup
    dd-dashboards-backup    | "DogFood (xyz)"
    dd-dashboards-backup    | Reinitialized existing Git repository in /datadog/.git/
    Saving dashboard to ./datadog/orgs/0374839/dashboards/zpa-ic9-3cj.json
    dd-dashboards-backup    | On branch master
    dd-dashboards-backup    | nothing to commit, working tree clean
    dd-dashboards-backup    | commit d439f29e20c8636adbecd93e3d135c4428d752cb
    dd-dashboards-backup    | Author: dd-dashboards-backup.sh <dd-dashboards-backup.sh>
    dd-dashboards-backup    | Date:   Sun Oct 11 05:35:32 2020 +0000
    dd-dashboards-backup    |
    dd-dashboards-backup    |     2020-10-11T05:35:32+0000
    dd-dashboards-backup    | datadog
    dd-dashboards-backup    | `-- orgs
    dd-dashboards-backup    |     `-- 0374839
    dd-dashboards-backup    |         `-- dashboards
    dd-dashboards-backup    |             |-- 3dr-g3y-duz.json
    ( ... output truncated ... )
    dd-dashboards-backup    |             `-- zpa-ic9-3cj.json
    dd-dashboards-backup    |
    dd-dashboards-backup    | 3 directories, 21 files
    dd-dashboards-backup exited with code 0
