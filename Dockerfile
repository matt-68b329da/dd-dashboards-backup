FROM amazonlinux:latest AS dd-dashboards-backup
RUN yum install -y -q deltarpm
RUN yum install -y -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum update -y -q
RUN yum makecache fast -y -q
RUN yum install -y -q which curl jq git tree
RUN git config --global user.email "dd-dashboards-backup.sh"
RUN git config --global user.name "dd-dashboards-backup.sh"
COPY ./dd-dashboards-backup.sh /opt/dd-dashboards-backup.sh
ENTRYPOINT /bin/bash /opt/dd-dashboards-backup.sh
