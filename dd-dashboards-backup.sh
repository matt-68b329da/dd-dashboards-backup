#!/bin/bash

## Makes local git-versioned backups of every dashboard from every accessible orgs

for dependency in curl jq git date tree ; do which &>/dev/null ${dependency} || exit ${?} ; done

test -z ${DD_CLIENT_API_KEY} && exit 1
test -z ${DD_CLIENT_APP_KEY} && exit 1

URL="https://api.datadoghq.com/api"

_curl() {
    # https://docs.datadoghq.com/api/v1
    /usr/bin/curl -Lkqs                                     \
        --header "Content-Type: application/json"           \
        --header "DD-API-KEY: ${DD_CLIENT_API_KEY}"         \
        --header "DD-APPLICATION-KEY: ${DD_CLIENT_APP_KEY}" \
        "${@}" | jq .
}

dd-org-show-names() { _curl -X GET "${URL}/v1/org"             | jq     " .orgs[].name "             ; }
dd-org-ids()        { _curl -X GET "${URL}/v1/org"             | jq -cr " .orgs[].subscription.id "  ; }
dd-dashboard-ids()  { _curl -X GET "${URL}/v1/dashboard"       | jq -cr " .dashboards[].id "         ; }
dd-get-dashboard()  { _curl -X GET "${URL}/v1/dashboard/${*}"                                        ; }

dd-org-show-names
read -p "confirm account : press any key to continue or ctrl-c to exit"

git init datadog
find ./datadog -type f -not -path "*/.git*" -print -delete &> /dev/null

for org_id in $( dd-org-ids ) ; do
    mkdir -p ./datadog/orgs/${org_id}/dashboards/
    for dashboard_id in $( dd-dashboard-ids ) ; do
        dd-get-dashboard ${dashboard_id} > ./datadog/orgs/${org_id}/dashboards/${dashboard_id}.json
        printf "Saving dashboard to ./datadog/orgs/${org_id}/dashboards/${dashboard_id}.json\r"
    done
    printf "\n"
done

git -C datadog add .
git -C datadog commit -m "$( date -u "+%Y-%m-%dT%H:%M:%S%z" )"
# git -C datadog whatchanged -p | cat
git -C datadog log -1

tree -A -C datadog/
